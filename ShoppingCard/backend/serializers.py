from authentication.auth.models import User

from backend.models import product, Orders, OrderItems
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = product
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('id','user','Total','Status','PaymentModel')

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = '__all__'

