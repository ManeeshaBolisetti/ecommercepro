from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from rest_framework import permissions, status, generics, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from .models import product, Orders, OrderItems
from rest_framework.filters import SearchFilter,OrderingFilter
from .serializers import ProductSerializer, OrderSerializer, \
    OrderItemSerializer


#
# class ProductPagination(LimitOffsetPagination):
#     default_limit = 2
#     max_limit = 3
@api_view(['GET'])
def productData(request):
    profiles = product.objects.all()
    serializer = ProductSerializer(profiles, many=True)
    return Response(serializer.data)

class ProductPagination(PageNumberPagination):
    page_size = 5

class profile_details(generics.ListCreateAPIView):
    def get_queryset(self):
        return product.objects.all()

    serializer_class = ProductSerializer
    pagination_class = ProductPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('id', 'Title')


class ProductDetails(generics.ListCreateAPIView):
    def get_queryset(self):

        return product.objects.all()

    serializer_class = ProductSerializer
    pagination_class = ProductPagination
    filter_backends = (SearchFilter,OrderingFilter)
    search_fields=('id','Title')



class ProductDetails_update(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return  product.objects.all()
    serializer_class =  ProductSerializer


# class OrderDetails(generics.ListCreateAPIView):
#
#     def get_queryset(self):
#         return Orders.objects.filter(user=self.request.user)
#
#
#     serializer_class = OrderSerializer



class OrderDetails(generics.ListCreateAPIView):
    def get_queryset(self):
        return Orders.objects.all()

    serializer_class = OrderSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('id', 'user')

class OrderDetails_update(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return  Orders.objects.all()
    serializer_class =  OrderSerializer


class OrderItemDetails(generics.ListCreateAPIView):
    def get_queryset(self):
        return OrderItems.objects.all()

    serializer_class = OrderItemSerializer


class OrderItemDetails_update(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return  OrderItems.objects.all()
    serializer_class =  OrderItemSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
@api_view(['GET'])
def Welcome(request):
    profiles = product.objects.all()
    serializer = ProductSerializer(profiles, many=True)
    return Response(serializer.data)
