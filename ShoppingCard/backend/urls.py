from django.urls import path

from backend import views

urlpatterns = [

    path('accounts/profile/',views.Welcome),
    path('prod/',views.productData),
    path('prodcreate/',views.profile_details.as_view()),
    path('products/',views.ProductDetails.as_view()),
    path('products/?search=', views.ProductDetails.as_view()),
    path('product/<int:pk>',views.ProductDetails_update.as_view()),
    path('order/', views.OrderDetails.as_view()),

    path('order/<int:pk>', views.OrderDetails_update.as_view()),
    path('orderItems/', views.OrderItemDetails.as_view()),
    path('orderItems/<int:pk>', views.OrderItemDetails_update.as_view()),
]