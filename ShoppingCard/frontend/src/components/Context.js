import React, { Component } from 'react'

export const DataContext = React.createContext();

export class DataProvider extends Component {

    state = {
        products: [
            {
                "_id": "1",
                "title": "mobilephone",
                "src": "https://www.sathya.in/Media/Default/Thumbs/0038/0038140-vivo-s1-pro-blue8gb-ram128gb-storage-250.jpg",
                "description": "UI/UX designing, html css tutorials",
                "content": "Welcome to our channel Dev AT. Here you can learn web designing, UI/UX designing, html css tutorials, css animations and css effects, javascript and jquery tutorials and related so on.",
                "price":10000,
                "colors": ["red", "black", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "2",
                "title": "Laptop Acceries",
                "src": "https://nmgprod.s3.amazonaws.com/media/files/f2/09/f209dcc76d120a2e4f3388dd462ce0a9/cover_image.jpg.760x400_q85_crop_upscale.jpg",
                "description": "UI/UX designing, html css tutorials",
                "content": "Welcome to our channel Dev AT. Here you can learn web designing, UI/UX designing, html css tutorials, css animations and css effects, javascript and jquery tutorials and related so on.",
                "price": 19,
                "colors": ["red", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "3",
                "title": "mobile bag",
                "src": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhAPEA8WFQ8RFRYWFQ8QEBUVFRUVFRUWFhUWFRcYHSggGBolGxYVITEhJSorLi4uGB8zODMtQygtLi0BCgoKDg0OGhAQGislHiUrLS0yLS0tKy0tLi0tKy8rMi0tLy0tLS0vLS0rLS0tLS4tLS01LS0tLS0tLS0tKy0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQECBAYHAwj/xABHEAABAwEEBQgGBwYEBwAAAAABAAIDEQQSITEFBkFRkRMiUmFxgaGxMmJyksHRBxUjM0KCsiQ0osLh8BRTc/EIQ0Rjg7Pi/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAKBEBAQACAQMDBAEFAAAAAAAAAAECEQMhMUEEElETIjJh8CMzQnGR/9oADAMBAAIRAxEAPwDuKIiAij9Oaas9jidaLTIGRjDIlznbGsaMXOwOA3FaTY/pMtFpN6xaDtU1nBI5dzmxg0NDdqC1xwOF7toidOjIozQemWWlriI5IpWECSz2iMsljJyqMi00NHNJaaGhwNJNECIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIKOcACTkMyVz06dtekpZI7LL/h7FEKvtFOeW441zBNCQBQgCpOxb9aoQ9j4zk9paaZ0cKGnFclMkuj7PpTR84LJJ7PLyEwwa94ie0XT61RTccDmseW3cnh6Xoccfp8mckuck1L1/3debEd9GthdpS12i0SOe7R9nNGiQ1fKXVuB788G89wFMS0b69Xg1bhixs7pYT/25nlm/7p5cz+GuJWlf8PzmfV84b6QtT73fFDd7qU4FdOV5x4zw5s/V82XfO39b6f8AOzFs8Tqh0gHKtF2+0EBzTQ5HLEZVNN+KykRXc9uxERECIrI5Wuxa4EVpVpBxGzBBeiIgIiICIiAiIgIitbI01AcCRmAcu1BciIgIiICIiAiIgIiIC8bXZI5WlksbXsP4XtDhwK9lFutEzpJi17GxQG6WGMue53JNkqX3gGjntwun0TjjgTLZdxrFh1InsFpltOiZo22eenK6PtIfydQcHRSNqWUqaC6QKkZUDd0sjpS0GZjGv2tjkMjfecxp8FAQ6QtTBBJIJOTpI+dk8cIkDAYmhzeRcWgNvOfmSQHClaL0g0hK5kM4tDbjmWXmFrCyQzEAlrhjeN4UoaZYKLdL8fHeS6jYkUQLfNyrIgwESVcJKGjWRvuy3uuhjDd5eTSjSpdSzFbI8NBccgCSeoYlXKN1hmu2eU7wG+8QD4VSpxm7IhLZpcyXgfQNRc2EHMHesOxyRxV5JjY60rybQ2tMsu08VGsfgrryw29SceMmtJv62f03e8U+tX9N3vFQl5VDlO6j6ePwm/rR/wDmO94p9aO6bvfKhbyXk3T2Y/Cb+s3dN3vlVGk3dN3vn5qEvJfTZ9PH4Tf1k7/Md75+afWbum73j81C3lW8mz2Y/CTtemC1rnOeaNaXGrj3LRtC64PdLByRaI3ysbc5FrOY54Dm1FSKgqR1lmIs1poaG4/EUwpGTh3rQBAWubaI20itDTKxo9FkrT9rEN1yQGg6JYdqhTLGTpp9JWOYPY1wNa54UxBo4EbwQQvZQ+r0tXWpmxsxc32ZGtcPG8phbTs8+zVERFKBERAREQEREBYsmjYHSCcwsM4F0TFjb92jhS9StKPdh6x3rKRBi2HR0EIuwwsjbjzY2BoxpXLfdbwCtj0TZmuje2zxB8TQyNwiYDGxoo1rCBzWgEgALMRNJmVx7VYI2ihDRUVoaZXiCeJA4K9ERAoDXR9IAOk+n8LvjRT60jXC2X5QwOq1gpQE0vVbU7q4U7u1VyvRrwY7ziHYVfVeTFfVZPTXVSqtRBfVKq2qVRC+qVVqAoL6qoKsCqgidZRes8rRm5sg7TSlO3YonUS2Nsz+RtPImKSkrJJmi5HIwgONS03C+MFhOOLWKV0++kR9knHrP9FrGkrKJI2FxIayJz3FudGuYBxc5o71G1M8ZY7foBovTPaKRvuXSBRrqN5xYOjU4HbUKZWgauWytmjtM07g6zMjbVz6NAAHLAAUDg5tKVBILQa1W82Sa/HG/pta7iAVrhejzuSar2REV1BERAREQEREBERAREQERYukGOuhzX3Swl12nNfzXNuv20q4HCmLR1hBpuk9O6RJlMMkIaS5rIXwuDmCmD3Scpzj1ANx7KnX7NM9zSJWgTMddeWGrTheBAqS044gk7OxXyWtwNCdgzGYIrt3g171gaBc8wlz2gOMsxJH4hyjqOPdQdwXPba9Pj48ceyWar15tV6lqrVFQogqq1VqVQXVVVaFVBcFdVWBH5HsQQusLvsj7DdvXvUKZi2WzNGRYQQPxAkYEbcQD2gKX1mPMeOoDwBUJKK2mzj1R5hFa2SwT1YRca66WG470TR4NXDaBUGm2lF12zk3GE53RXtouM6tNcICJWH/ABF6UvkcSDdLW3W0pSlWuI3d67PB6LeweSvxuT1XheiItHIIiICIiAiIgIiICIiArJomva5jgC1wILTkQRQg9yvRByvXbQFns8rLkTn321JmtNpeRQkUBMmAWLYjzI6Ma0CoDGVugAgbSSTtJJJJJJU/9Ip+2iG6OvFzvkoKzN5rOw+JKwskvR6XDPslZIVysCuqjZVURUqoF1VSqoqVQXgq4LzBV4KkXhUkyPXhxwQJIcO8eYQa9rKcHe0B4H5KMu1tUI9Vv6j8ln6xu5v/AJfn81j6NjvWsHoxV/V80VrZbLAS110FznNIAzcSaACgzrXwXU4fRb2DyWlapxAzM9Wp4B1PMFbwtMHH6rL7pBERXcoiIgIiICIiAiIgIiICIiDnOvklbVToxtGfaf5lE2V9WtpjTCgx6wszW+S9ap+otHBoHwKxBGRYS5po51qNHYVoIIxTjXgsb3r08L7cMV5lG007cFUSjeOKiGTWoZSA9rR8KK8Wm1bSw/lPzUNEnaJrrHO6LSeAqtrsurEckccglcL7GuyBHOAPVvWiWe1Oc9kc7GCJ9Q92WF0mmO+6BwUr9YiIiPlnR0aCA2RzWhoN0AAGgHUNyhlye6/jdNodqhun4x//AEo3TGgXQM5QyBwvNaAGkElx/wBz3KPs+sjje/anc0kc+XOm0Y5Z8FZpfTsjmMa+W+wuc4CgJPJRvkcGuaM7rHZ7RTal6RTG8u5uzTzVaqJi03G8VDZKH1R8l6jSkW947Yz8kdKTa5XuPo9te4A/GiifrOLpnvjcqSaTaQWxguJ/EW0AB6yMlIitOvqxh3yDwoD41XpoR37S/rjpj3FY+lWEMszTmcSd5JqSe8q6wOpaSdlWDKvpXR8UV8t81ctQbaIqnAm771QPFwXQFzOxYOG8HZiKjaCumLTBx+qn3SiIiu5RERAREQEREBERAREQERedokutc/otJ4CqDlmm3Xpp3Y4yOIyyvGlFmPipYLN60s7uEhaPABR8wzJpnv3fHDwU9pqG5Y7Aw58nU+0Qwu8SVjPL0sulxn87NfYxX3Ear1Vs8LtCHDMb8j1HqXlaNKTg/u8Dh60cgPFrwswhWGNEWSsD64cM7Ez8kjm44GuIO4cAsee0h8bY2Wbk7mDSZQ8CoumgLBQ0JFa7SpXkQqtiCVHsjF0bZA1gFFlcgNy9WhXURZj/AOFbuVws7QDhs3LIAR+R7D5INd1kbz7MOs+QWFB988+uzwLVnawis8I3fJYNnxlf/qAcP9lKPLcYTie05ZZHgungrl1nOI66ZdY28eK6bZXVYw72g8QFfByeq8PVERaOMREQEREBERAREQEREBR+n5Ltnl623feIb8VIKB1wlpE1nTdvpg0GuPaWqL2X45vORpEkJfzATV9GgkbXm6K7jUjgtn17AAs4GQv0HYY1G6Es9+0QimAdeOPQBcMPaa3ipHX8/u/ZJ5xrOfjXblf6uMaq1XrzavQKjoVREQEREFQqhUCqEFQknou7D5KoVJMj2KRr+nf3mIdR+Cj9H4yO65nfzKQ0vja2Dcz4hR2jDz675D43kVbXZn+gd4bl1Uz4LpuiH1hhPqNHeBQ+S5o6K4I9xY0g7aOaDj1VLvFdB1ZlvWdnUXDibw8CFfDu5vU9cZUqiItHEIiICIiAiIgIiICIiAtR1vmrKxlfQbu2uNT2ZNW3LQNLTX5pXYkXjTrDeaKdwVM+zo9NjvPaT1PgrJJJ0W0GG1x8xc8V5a/5wdj/ADYpjVWG7De2vcTw5n8pPeoXX486Dsd5tT/FfG75/wCfDWGr0C82q8LN2rkVEQVREQVRUVQguCPyPd5qgR+XePMINf0h++djQozRBrdPrV8HKRtp/a5DuYPio/QowZ2n9JRV0LTVnpDYZAPSgaCd10A+N8cFOakT1ZIyuVHD9J8mq3TFkvaPgNMYmRO7rgafMHuUPqpbhHMA40DqtJ3VpTxDVftXN+fFY6CiItHEIiICIiAiIgIiICIiDytUtxj39FpdwFVzmuOOzPHdt/ot61hku2eU9QHvEN+K0SxtvPY2g5zmjsvGmHFZ5u30s1ja6Jo+G5FGza1rQe2mPjVanr8efB7LvMLdFpOvx+0h9k+ZVsuzHg68ka21XhWNVyyeiuRURBVFREFVVURBcEecO8fqCoEfl3jzCDXLef2ic7o/g4rD0OMGfm8lkaSP2trO6Mj+D+q8dE5M7HeQRV3awRh1nia4Va6JoIO0FgBC51pGyGCZ8fRdgTtBxHEEcepY0Wt9tj5rZatbgA6NpFBgMhVec+sD7VIOVa0SBtAWAi9Q1xBOeJV7ZY5+Ljywy69nTdA23lYWOJq4c13aNveKHvUitD1V0q2F5DzSNwxOxpGR7Mx3jct6jeHAOaQWnEEGoPYVbG7jm5sPbl+lyIisyEREBERAREQEREETrV+6y/k/9jVo+iD9vB/qR/raugachv2eZozuEjtbzh4hc1gluua4ZtII7jUAd9Fnl3dvp/wsdYWka/8A3sPs/ErdY3ggOBqCAQeo5LS/pAH2kB3td4H+qtl2Yen/ALka01XArzBV1Vk9Jcq1VlVWqC6qK2qrVBcitqlUF4KP2doVtUccu0INa0l95bT6o/QF56N/B7L/AOVXaS9K2nrYOIYF6aIjq5jd7R/E6nwRV1t2qFhe0XoKOoMWyPbj2A0UdaPo+s9Q6KaRjgaipa9vCgPitxRbajzJy5zy5XPA6KR0bhzmGnb2dRGPeFSwawTWN9xpvQuxEbsqHonYQtq110XUC0sGIweBu/C74cNy0yaESNuHDcaZdVFneld2NnJh1dG0HrHBaRRpuybY3Z/lP4lMLihglgcK1BwLXA5jYWnauh6qaycqBDMftR6Lj+LqPX5+d5k5eTi1N4toREVmAiIgIiICIiAuX6csJgmfHTm1qw72nKnZiO5dQWLpDR0M7bs0YcBlmCOwjEKLNteLk9la3oHWeGOz3J30dEKNHTb+EDrGXcCtd03bZbRS1lhbATcjccjTGjd4zNcq13LerLq3Y4/Rs7TXbJWThfJp3LJ0to5k8ToXYA5ED0SMiFGrpacmMz90jll5VvKVteqFtZ6LGyDeyQA94fSij5NC25vpWOT8t1/6CVn7a7Jy43y87yXl5Ps9ob6Vmnb1us8oHG7RY77SG+kbvtYeaaWmUZ15VqsBtqacnDivQT9ahbbLqq1WKJlcJkNsiqq44d4814CVXF+HDzCG0DpYY2vrdH/IpTVaC9aIm+vE3urfPmsDSLRykrCRV5Y4D1QBU8QVt/0d6PvTmQjCMF3Y5/NaD+WvBTO7PO6xtdJREWzzFsjA4FrhVpFCDkQcwudawaJdZ5MPunVLXk/wnrHy610deFtsjJWGOQVaeIOwg7Cos214uT2X9OcWW0MLeRmF6I5HC8x3Sb8Rt8DhS2Z0T8HVGbXjJw2Ef3gpLTWhZLOSSL0WyQDwIGRUa2QZH+/6rN2TV+6Oiat6T5eIXvvGUDuvcf73KXWrak2U0fL+A80Deagk91KcVtK0nZw8kkyuhERSoIiICIiAiIgIiICIiAhCIgxJtGWd/pwRu9qJp8wsSTVmwu/6SIewwM/TRSyImWxr0mpdgOULmne2aTyLqLFk1Csp9GWZvY9hHiwra0Uai05M55aTL9H/AELWR7cQd5OCxJNRLSPRnid7QezyDl0FE9sWnPn8uZS6n20uaDEwkf8AMEgLe3GjvDYt51e0QLLEI63nuN5797urqGSlESYyIz5cspqiIilmIiILXsBBBAIOBBFQR1hQNs1QsshqA9nVG4AcHA07lsCJpMys7PCxWVkUbImCjGCgrie09ZOK90RECIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiD//2Q==",
                "description": "UI/UX designing, html css tutorials",
                "content": "Welcome to our channel Dev AT. Here you can learn web designing, UI/UX designing, html css tutorials, css animations and css effects, javascript and jquery tutorials and related so on.",
                "price": 50,
                "colors": ["lightblue", "white", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "4",
                "title": " Colleage Bag",
                "src": "https://images-na.ssl-images-amazon.com/images/I/71lA6cgCWRL._SL1500_.jpg",
                "description": "UI/UX designing, html css tutorials",
                "content": "Welcome to our channel Dev AT. Here you can learn web designing, UI/UX designing, html css tutorials, css animations and css effects, javascript and jquery tutorials and related so on.",
                "price": 15,
                "colors": ["orange", "black", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "5",
                "title": "Python Text Book",
                "src": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhIVFRUWGBYVFRUWFRUWFhgWFRUWFxcYFRUYHSggGB0lGxYVITEhJSorLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGi0lHyUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABQMEBwYCAf/EAFAQAAIBAgMCCQYJCQUGBwAAAAECAwARBBIhBTEGEyIyQVFhcoEHNHGRsbIUIyQzQnOhs8EIFSU1UmJjwvB0gpKi0UNTdaPi8RZFg4TDxOH/xAAbAQEAAwEBAQEAAAAAAAAAAAAAAQIDBAUGB//EADgRAAICAQIEAwUHAwMFAAAAAAABAgMRBDESITJBBSJxEzNRYYEGFCM0QnKhkbHRJGLBJUNzosL/2gAMAwEAAhEDEQA/ANxoAoAoAoD4TQBUZBWn2hCnPlRfS6j8ao7IrdjItxPC3BJzsQnhc+wVHto9ivHH4irE+UbBKCVLuACSVUWAHSSTUcb7Rf8AQo7o/EQ4zyx4ZeZEzeJ9mX8auo3vaH8lHqIioeV+WU2ihVe1h/1H2VPsL33SKvUfIb7T2vtEYM4xcTHlALNGEytYPkIVgDc9PRWM6rU+czaLclk7MYpxblE6LvsegVtHbmXR7G0H7D4VYklXaR6V+2gPa7SXpU/ZQEq49OsjwNASidf2hQHsEddAfaAKAKAKABQH2gCgCgCgCgCgCgCgCgM18rnDDE4MwwwZV45HZpNc65GUWTWwvffWkK1PKZhfNxXIzFuEeKl+cndr9ZJ9tbQ0NPfLPNnfZncq8IZXWGPlNcsbkEgkWO+3R2VD01UX5Ym9c3Jc2I4nvv1rojwozkh/hmvhpe4fYaliHIRpRFXuW9mLZx1VWSwXi8mybRW+wpD/AA3+/rgu7no1dJ15W6qR+ynuiqR2RoR2qwPtqEM+WoQSxx9JoSeqEBQkHZhqGPbqaEnxcZIPpeuxoCRdouN4BoCQbU61+2gLGFxoc2sQbXoC3QBQBQBQBQBQBQBQBQGJflAn5Rg/q5vfjrehnPqFmJnGGk1312xaPLnFjbb2sEXePsNUe5evkhN+bJeLM4UcUpsWzoCDcDmFsx1YagdNVbSeDeMcxyNMG3yaXun2VLZVLkLsLBmdVzKmYhcz5sq36Wygm3oBq2cLJRLLwNPzeYZ2iYglctyAy85VYclwGHO3ECob4lkJOEsGr40foKTuP9/XBbuz1K+k6/Dm8cf1cfuCs4bIseZEqwOHnweJmxmLWHEtFxRRrZnscyDQAGw3dXTXmuM52SSljB9DCymnTV8ValxcixHtuZsPgZ89s2IEM9gtnGYi5BGlwvRbea09vJwi898GT0dSttg1tHK+Qwxe08WcTi4sOFfiUi4tCFHLcoWuxIvyc+81r7SbslGPY51p6Y01Tszzbz6FDZ/CPaDSmJsIrFHRZsl7xhzvNmIOlzpcaVlXdbKXDhctzo1Gi0calNTaysofbK2sZjibpl+DyPHo184UE33cnd210V28WeWx5+o0vs3DD61kq/8AiqFcImMdWVZCVWPkliQWFr6DcjG/VUfeIqtTNF4bZK90RfNbsNjcI8Pig5iLDIMzKw1C9Yykgj7d3XSvURsz8iup8Ps07WcNPllFobSh4pZuNURPlyu11Bzc3fuJ6jWnGsZOf2FnG68c0WiKuZF/Z8WVxffY3+zSgGlAFAFAFAFAFAFAFAFAYl+UIvx2DP8ADn96Kt6VnLOe98jitl4lvgOIXNGUzRkJmXjlPGpdilr5DZRmJsDpblXG66kc7j5A203xEXePsNX7mEVhBgLfBMQLS5m4stp8RlSRbcoa8ZdhYHSxOl7EVa8yNYvyEeGW0Eo/dPsNJcmRF5RTwSZnReKaa5txSlgz6HkgqCR4VL2Kw6h3jFIxb3jMRIjbi2tePNGpyEjnEX5x1O86k0h0kz6zUMf+o5O4/wB9XFb1HoVdB1eA1hiP8KP3BWMOlFycirA5bYqfpHaA7IT/AJP/ANrjq97YevqH/paPV/3EMK/oZW6Ypw//ADgv81YxX+nT+f8Aydkn/wBQcfjHH8HScGLPicfL+1MsY/8ATX/qFdOn5ylL5nna/Maqof7c/wAnjg359tHvw+xqin3k/UvrfytPoyHgy4B2iCQDx8uhIH0WqtMkuP1ZprItulpdkJcCoOE2SCLj4Sbg/WtWMemv1O2zldqP2/4HeNA/Ob6b8BJe3fP+lbWe/focVXPRxf8AvRzOGxGfY4XpinRPAurj3z6qxhLNHoz0LYcHiDa7xf8AY09Y8pLHf0dnbXpny7JsFzx6DQDGgCgCgCgCgCgCgCgCgMX8v/z2C7mI96Guijuc2p6TgcFDlwc7hUPKSPNxMpkGZ0b58chV5B5J11HWL6bTRkudZ62ifiIu8fYa0zzOfZEuGNsFLyt7qoU4iwIJDG2Gtq10XlX1Gb9nWufOi6ea2EIvh5e6fYa0lzeSkOSwKdnRhpEUhGDMBlkLhDfQBjHy9f3dao3yLQXmHeIjC4uRFAVUYIqqJQFVQAq2m5YIFrg9N7aWpHpJs6jVMZ+o5O4331cdnUd1PSdTsrzeD6mL3BWNfSjQtAVcHA4ufFptHGfBIhKx4vOCBovFra12XpJ6681+09tLgPoYLTvSVO5tb4wMU2M8WyZYZAM/FyyMoN7G/GBbjeRYa9dbupw07T3OP70rddGyO2UixwAUnCcYd8ssrn1hP5KnR+7yV8XaWpcVslg88HPPto9+H3XpR7yfqTrvytP1EGD4Nw4qTGySNIrRzy5chW2l21zKemueNELHNv4noW663TqmEMYcUfcAb4XZHbiW+9akV5K/UWcrtR+3/A3x/wCs3/sEnvtWs/fv9pyUfko/vRx0DZcK8fRJHgZ1/uycU/2qK59o49GelPzWcXw41/GTXpN59Neoj5V7kmE548akgYUJCgCgCgCgCgCgCgCgMZ/KAX43BdzE+9BXRR3OXUvynEYSPNgJeSSoa5OSUkPmhyESDkKoBcMDqSyaHQrq+swjL8NlTHn4iLvH2GrLco+aL2FcnZ8pzaK4UpxpCnO8TZuJy2eQZLA5t2bTk6w+tF4rFbIsMfk0vcPsNaSM4cxbsKXLPERbnW1Zl5wK6MoYqddCAdbaGs5dJeHUOMWuXFsl1bixHHcSGXSONEAeQquZxazckWNxbSpj0IW9Zq+JT9BSd1vvq47epnbT0HUbBTNg8OeniYvcFYw6TQt2q/cHL7D/AFpj+7D7qVyVe+kenqfyVP1H+1E+Jl+rk9w10WdDRw0e9j6oTcBB8gg9D/evWWlX4SOnxP8AMzKvBzz7aH1kPutUUe9n6m2u/K0/Ui4Mf+Y/2ib3TVadrPVmmt3o9EJ9nea7H/tDfeNWMeiv1Ou332o9P8DjaH60k/4fJ77VrJfjv9pyUfko/vOU2nCVwezpQNHiaFj3ZlkT2NXPdngg0elQ1K6+L7Zf8YNbnGpNeofKdz7hByr0JL9CQoAoAoAoAoAoAoAoDHPL8fjcD3cV7cPXRp92cuqXlM+wDSnCzBI4THnVpJM4WcEWygLxgLoNSAUYAlyNRpvhceWc+XwYR82uPiIz+9+BqXuUXNH3CCZsHLlnAijZWeDK1yXKgNmy5VuVA5wJybuurxxJl1nhZJhfN5e4fYavJlI8hPsqZklRklETBgVla9kPQxyqxt6Aar2Lx6h/NhpUxTLO4eQZCzAMoN1FuSyKRpYaqPtqYdJSzKkatiWtsWTqysP+aK47d2d9L8p0fBeW2Fw3UYYvdFYw6TQY/CI3YhHVmHOUMCR0agbqlNN7l3CSWWhXs/ZiLiZ8SsuczBFZBlIXIAN4PTbprOEEpuS7m1t03VGuSwo7fUZYiLjEdARylZCd9iykajx3VpJZWDKDcJKWO4v2Hso4fDRw5xJxeYFgMoOZ2bm3Nt9t/RVaoqEFHJfVXO6xzxzfYrbK2Q0WIxMxYMJ2RlUAgrlBFiTvvfoqKquGTlncvfqva1QrS6SLZGxpIvheZlPHyySJa+gcEAPpv16L1FdXCpZ7ml+q9o6+XSkv6Co8GsSuCwsaNH8IwzmRdSY2JZjYMyjoKnUb1t03rL7vJQSW6OmOvreonOa8klh43J9jbFxrTTYnFlOMaBsPGqEWIbXW2ii/jqeoXV1WcTlPfA1Gr06hGqlPhT4nncixfBqdtmYfD5AZ4XRrZlto7ZrMdOaxNJUN1xXwIhr4R1U7FtJNHatvrqPK75PED/GADm2PiaAYUJCgCgCgCgCgCgCgCgMb/KCHLwJ7MV7cPXRR3OXU9JwmyY1+BYlsrliyrmVmAsjRmzrxgDAFwb5GtfUjMtbPqRivdkO2D8ni7/8AKas9zOOxLgcnwGW6Rs5a6k8UJFVGizFLoZCvLF+ULWFgRnqv6kaY8jDB+by9w+yrsyjsLNjwgzxAqGUuoKlQwI6QVYgEekgDpNUlsWrfM6LHMpxblECKchUAoykFFsysjFWBG4gm/YdBaHQVtfnNMxv6lk7rffCuO3dnfT0j/gyfkeF+oi90VjDY0OW2bhFTFR4hbh3x88Da6GMo53ddwP6tXDFJS4lvlo9+yxupweygmvU8cF8IsMuHmjJDTrjRIL6HiSSmngPVUVwUWpLvkaqz2kZVy2XDj67kOxmbDxvLG7Zp9ny4qS5/2obRh28o1MPJt3i2XuSukoySxGaivQ9zbMfD4TFIDIYXjwkqu3+9kkXjcpH931ip9m4waT5PBRXxtug2lxLiW3ZLkMcMDh8LtGLjHdYC6xs55QDwI1rj95jVlmNc0uxjNe21FMmkuLfHqLNqY102VhXzuGSfK5DNmPFma4Y3ueb01lOySpi/mdMKYT1tkUlhp4+pPtraMhn2nkkdQkMQTK7AKbx3K2Oh1Ooq07ZZn8khp9PBQoylzk8kqbYkefCgSOAcA0jgMbM5iezMOlgV31ZWtyivkZy00I1WPH68L0yUsRt3EIrDjnudmRyqcxuJhqXH7xHTT2rTx8iy0tUlxY/7jX0HD7SxRx11kPELOmEaELccvDiUyluizECtVKbmsbHLKqlUYa82G8/XGDrsHzx4+yupnkp5GVQWCgCgCgCgCgCgCgCgMc/KE52B/wDc/wD166KO5zajY4TZa/Ipjyb52VWKqSoyxtIoYtcZ1UDceZb6Wm36kYL3bItrp8mj7/8AK1Wa5mUWfMBMowUimVQzObJniDacWbBCvGFWy6lWAuiCx1qn60b/AKGS4Fb4eXuH2VqzniyjweVjPEF35wObnvvuMn0iRcZem9VfSTHqHWPXLimAAUAIEQKFyKFAEeUEjk2te5vv6aR6BZzmabif1JJ6G++FcVr5no1coj7gv5lhfqY/ZWdexZ7Cjg1sYPiZcQzseLxM4SL6Aa9s5132bsrlpqTcpt92evrNVJVxqS3S59xXwUw+WdGYluMhxnFgnRGSXKwUdoufE1hVFKWfkzr1dnFXhLpcc/Pl3IlPyZP+ET+1ansv2sY/Ef8A5UetoQBVxwUnlwYBzrezNIo07NAaSXKXoiK586m+zmTSR8Xs7HRsWkl+FCGRjfNIzNCF3knVSBarLy1SS5vJVSU9VXPGI8OfTfJWnUzYJY3QozY+cFDvQyRTyZTp0Fvsqr80OFrHMupKvUcUXlcC/wCEVcHLnixUpHzsCOb9XwrixfwSqx5xk/ijWzyThBfpbX/rkm2Ovx5/hpi8OPRDEbe/U1LzfRorqH+F6uMv6so7bQ8UHH0cFglPdlSdD9pX1VFm2fQ007TnwPvKT/pgfHMMYWWRlH5wWNkViFcPg4Tyx02ybu01un5l6/8ABxvDpaa/R/8ATO7wnPHj7K7meFH4jKqlgoAoAoAoAoAoAoAoDH/yg10wR7cR7Ia3o3ZzajY4HYik4LEkRo4DLnZsoaNSFClSY7tc30EgN1UgWzZtv1owb/DZ52wfk0ff/latHuYR2LGzWf8AN2ICAFDIvG5mdbX4vKYxzGbfdTytxF7VRpcaNovyM8YL5iXuH2GtGYRFuy41M0QbLlLqDmCEancQ/J13crTXWol0smp5nzGm0bHFtlg4gWS0VkFgVBuOL5BB3hl0IsemqJ+U1ksTNSm/Uknob70VyT6jtq6R/wAFB8iwv1Keys63yNC/svALAJCCx4yR5TmtoXtcCwGgt6ahRxk0suc8N9lgX4PYEcZhIdyYDMVvl5XHklg9hqBfS1vGqeximn8DSWrsfEn3x/B82JwUw8BlK5m4xSmVyCEiY3KJYDQnr6hURohHOO5e3XW28Oe39yPBcDMPHBLArSESlCzkqXAjYMiqctrC1t19aRohFcK7lrNfbZOM3jKX0Lk3BuF2kYtJ8ZNFiWAYWzw5coGnNOUXHsq/s4mS1diS+Sa+jPp4ORZy+Z7nEfCrXW3GcXxdt3Ntrbffpp7GJH3meMfLH03Kw4IYcRlA0gBiWHnLfKkplB5vOLE37KhUx2+Rd623Ofnn+qwS/wDhyBWMl3BYztzha+IVVfS3QFFvHfU+yiuZV6uxx4fT+NipNwXgZHjYyZXihgPKFwkBJQjTRrk3P2CodMWsBa2zPEn3b/ruSng/AcUMXZuMA3X5GbLkD2tfNl032qVVFz4iPvtvsvZZ5DrCDljxrRmCGNQSFAFAFAFAFAFAFAFAZH+UBuwXen92OujT7s5tT0mfbKVPgU4IjZlcMgkkClQQAzwpmu77hYqBYnViLDZ9aOZc62RbT5WHTv8A8prR7ma5LJNgEQ4Ga6xM6yAqXdVdVOQM0K3zOxsAbgC17ZiLLm+tF4862ecK1oZR+4a0nuZVrkU9kH46Lmj4xNWsFGu9iUcAduVvRUT6RX1jHaS5cbIPj9664hg8rckcosN6nev7tqzh0nRZ1Gpyj9CSehvvRXNZuddfSdBwQ8ywx6oUrGvpLjDEYlRz3VerMwX2mtMFW0RwY2JmyrLGzb8qurGw3mwN7UaeBlMobAEiz4lJJnkKmMgOLBRJnb4vTm2IXp1jNS9ikdxHh8bK7YmMzTgHEQx3zMpUPtCeJhA+hVTGqLdToQ2t6u0gnuWYJ5GbCuZZbrHgzYSMFczzNHIZUBtISqjnA2NyLGqvuSuxAuPaSDEZpMTlSSNJSnHiVUE8udognLIy25SXuFIuQptMlgRyzqMbIQuGKuSGljGYHnqY3Ott4Ngbbqqiz2Qn4DYxpYSzszFeLQ5iSbqmW+v7QAbtzXq01gzg2yzs3EcXh0LcbJy5VBVHmc2lkALWudwGp7KrJcyyeEWsNjg5sI5l0veSJkX1npqOHHclPPYv4TnjxqGXQxqCwUAUAUAUAUAUAUAUBlHl7W6YTvy+6ldGn3Zyat+VGe7KdvgGKVSy6i7CKWRCBluhYKY4SdDnJDaW3GtZdSMa+hlPFH5Onf8AwNX7mc9i/g3P5vxAU25anWKVww5F1DhOLhN7HMWubWsL3NZdSJq6Gins03glv+walvmEuRU2UCZogCReRBdVkZt/QsTK5PYpB6jUy6Sta8472zCwxrq8bxkZbK8rzMVyizcZIS1mHKyk8m9uiqwXlNLH5zTZ2/Qsnob70Vy2dR2VdI84JN8hwv1KVlXsXGMmGRrF0RiNxZVa3ouNKvnAwe4oEB5KqvaqgewVG42F2w+PaaeSeExXCRrqhDrHJOQyhWYgZXTnWN76VaWMciqzkWRbHxIMzcUuksbxjjF+MVMdPiTruTkyga9INWyiFFos4fYmIVsPpHlWPDLKc5ujYeRpOQMvLDFsu8WteobROHyJIti4pEk4to87BALs2VgJJiyscpIGWQagb6ZQ4WM2wByYdAw+JaMk25wSNk09Nwaqt2T2RV4PbF+Cq4zZi/ElrCwzRQRwkgdR4oHxNTJ5KpYLGDw3FRhCwNi5vu57s9vC9vCofMlckSmoJJcHzx40ZZDGoLBQBQBQBQBQBQBQBQGVeXn5vCfWSe4K6NPuzk1fQZns6NPguKYxhnBAVjFC2UaZijlhKGGlyoZVBJI1BGs+pGVT8mDxjB8mQ/v/AIGr9zF844Luz7fAMSctzmUA5JLLcpc8Yptc6CzWAvck5gKrLqRpWvIytgh8TIf3DeryXMpF8hbsxM00S5c15EGUhCDcgWtJyT/e066hvkTDqOg2lhlixbRooUDLuiWEXyi5VF0te9msL77a0h0C3rNi4NQI+BVHUMrFwVIuDyr6iuSzqO2rpK21dn4iKMNgJWQxjSA2eFlGuUKwuvTuIrNLBoXuCu3lxcOe2R1OWRP2T1i+tjr/AFqQJllxOpKga2GqAalrXvrbd2kgW6SbYRR5IjPiSLGWJTZtQydAIDEfs6o3Zmt20whlliSdwQBMh5RNi4zEZFsOSOvN0dINjQnJ8gdwzZsRuVhucgNcKGykAGx0sD29NGERzSqz64k8pgVWz2HZ6DmXQ0Iz8yqQgFzNKQM17oSu7LcjNrfTd1dFzVskcj7E0QOe8jFCDZgml9BY36Slt++1QRgieGPIeTMwzZNMm8KxIXTeMoHiNd9Mk4GDYxw2QQlrHLfNvs2U3OW27X/tUYLZwNMJzx41QuhjQsFAFAFAFAFAFAFAFAZV5fPmcKf4r/d1vRuzl1S8pkeGxzqrRAjKzFjyVJ5SNGbNa4BViLejqrqUU3k5HNxWEX8f5snf/A0e5XsUo9oMsRiCpZi3KIJcZ1CsFa+gNgbW3gVGMtMtGWE0MtnfMSdw+ypZWDyJ9nOgkRpBdAylhbNcA63UkZu7fXdUYyiU8McmeN5s0Y0NrnIseZhcZsiclSRlJy2F7mpisRwxKWZZNw4IeZp6X96uOzqO+rpGIqhocdsKHitq4mFSQsicYLdGa17eLA/3RQHWvshGbMxY33jr5KA5uv5tft6zU5K8JLFspACAW1A1uL3GSxGm/wCLXs39dMjhR6GyIupj6WPRu9VhTI4UWGwMZJut82/U2OobdfrA9Q6qjJOD6MBGNRGt95NgST1knee2oyxhEnFjqHqFSTg8hQBoAPQKgHkmgPD1IPWC5/gahkoYVBYKAKAKAKAKAKAKAKAyvy/eb4X65vu2rejc5tR0mNRiuxHBIb48/Jl749hqrLLYTp0VKKNj3Z5+Jk7h9lJFqxAhpEMZbK51S9isdzfeCHmad5/erhs6j0qukaDfVDQ5TZnL23Ow3R4fKe8THYe31UJO1oQe1oBXwl26uEh4xhmYnKiXtma19T0AAXJrWmp2y4UYai9Ux4mZhtfhHj5DeWWSIMMyqgaJSp6RaxYdtzXq16emO2GeLdqbpPL5Cg46YG4mlv18a9/XetvZw+CMfaT+LOx4C8L5jMmHxDmRZOSjtq6ta4BbewNra63Irg1WljwucFsehotXLi4Js0k15p65GTUgic0BJgef4H8KhkoY1BYKAKAKAKAKAKAKAKAyzy/ebYX68/dPW1O5z6joMZSu1HnS2GuP81Xvj2VVl47ClasijHmzvmZO4aiRas5+M1CeAxpsrnVZ7ELc3zgf5mnef3q4bOo9GrpLm19ox4eJpZCAFGg6z0AVQ0E/APZckccmInFpsU3GEHesYvkB6jqTbtHVQHVKb0B7WgOD8qbgNhCRmAMhK9YBjJHiLjxrv0CzxpfA8vxGSUoZFPD/AIQwYriRBchMxZipWxbKMgB9GvRurfSaedbbmc+s1ELcKHY49jXacIw4Mee4b66P3hWGof4cjfTe9ibk1eDsfRkTmpBGxqQTYEcvwPtFQyUMKgsFAFAFAFAFAFAFAFAZZ5f/ADXC/Xn7mStqOowv6TGIzXYjzmNcb5sO+PZUMtHYUoalFWPdmH4qTumoYiI2TpqcBMv7KPKoyFubbsvGtDstpVALJmIBva5cDW3pris6j0qukm2NswYlIcXimMrlQ6RmwhjN9CqDnHTe16yi8o0OkJqwPSmgI8ZtCGIAyyxx33Z2C39F99TGEpbIznZCHU8HIcKsbszFGPjcYw4vNYRKzXzZb65CPo126eN9eeGJwamentxxTEJk2Kn0MXN6SV/Fa6P9U/gjnzpI/FnMbRkiaVzChSInkIxuVFhoTc31v010w4lFcW5xycXLMdi1wU8+w31ye2s9T7qXobaVfix9TW5MbJc69J6B11+V2eM6tWtKXJM+2jpa+FFyCbMt/A+mvqvDtYtVSprfZ+p591fs5YPVr13mJZwZ5VhuAPtGtQy6L1QSFAFAFAFAFAFAFAFAZj5elvhMP9f/APFJW1HUc2pflMSQa12o4GOJx8mHe/Co7jPlFJS1TgjORzsv5t+6aqyUK1FxViha2UvLtUtFk8myD9TS+hvfSuCzqPSp6R7wUHyLDfVj2msK+k0GlaAhx2KWKN5W5qKWPbYbh6d3jUxi5SUV3KWT4IuRjO0cc88jSym7t6gOhV6gN1e/XXGCSifOWWOyTbKbGrmR5JqCSNqgsN+BcRfH4YDofMewIrMT9lc2slw0yOnRrN0TTnOp8a/GbWpWN/Nn3sFyWS3s1ucPQfbX0v2cm8TicOuWzLtfUnnonwPO8PxFQyUX6gsFAFAFAFAFAFAFAFAZp5d/NMP/AGgfcy1vR1HLqugxYx9PrruwebkaYnzYd4VR7l+wuC3q2DPOBls1bRyd01WSNYPKFWHa4FStjOawxpgI9QRV2uRSD5msqf0LN6G99a863c9inpH3BPzHC/VL+NYV9JqNTWhBzXlCny4MgfTkjTwuXPuV1aKObkcOvlioy1q9k8MjJqGSuewyw/BzGyaphZSOsrkH+e1YS1Fa3ZvHTWy2Qxh4AY5ucsUQ/iSj2IGrms8S08N5HTDw699jrODfByPBBm4wSzuuUuBZUQ6kJ6SBrvNhur5Xxvx6uVbhU+bPb8P8OdcuKQwNfCdj30XdkpfP2W/Gvqfs5vM8/X9i2TX1XY88sbP5x9H41DCGFQWCgCgCgCgCgCgCgCgMz8vXmeG/tS/cT1vQ/MYahZgzHYGrvTPHlyLu0eThv74tVJrBtVzQviO6rJmMkPcEgMT901E0WqfM5vCnkiqxNLF5h1so8oVoYJczVyLbGm9D+8tedd1M9fTvyD3gp5jhfqUrCvpNxtWgOS8py/JYj/HX7qWu3Qe8foed4l7tepmbGvWPFO68nWykCNi3UM2YpCDuGUcpx23Nr9Fj11854/4n91rwt3/c9vwrSKfnZ10s7nex9dh6q/OLtdqLZcUps+nhTCK5IgNc0rJS3ZqopdjwargujwxoSNNjLyWbrNvV/wBzX1/2eqxVKb7s87Wy8yRcmTpHiK+iyeeetn84+j8aMlDCoLBQBQBQBQBQBQBQBQGZ+XvzLD/2pPuJ61p6jG/oZjEDV3o8iS5DTa/mnoZfwpMvRuK0O6oiyJLmx7s1vi37pqZMrWc1hm5IqsTazccbJblCtDDHM1w/qaf0P7yV5t3Uerp+gfcFh8iwv1MfsrKvpNxtEt/RVgc95SYM+BYj/ZvG/hcofseunRyxacWug3T9TJGNe0zwcGo8ENNn4bt44+PHPX519rpN3RR9d4MvwRoa+QPaI2ahJ8Ck7gT6Betoae2fTFv6Djit2Sx4CRujKOs/6b69HT+DaixriWEYz1VcdmOYowqhRuFfaUUqmtQjsjypy45OTPWa2talT7gU5TEdIHhvoC9QkKAKAKAKAKAKAKAKAzD8oJrYCA9WKT7mcVrV1GdkcxMVgau6LPKmsDjaz2whP76/aRSfJDTrMhNE+6oiTJcx/stuQ/dPsqZFIbnK4GS4HWKzizqujzyPtjnlj+uitkzjZsKa7Gn9D+1K86/qPU0/uzoeDCXwWF+pj92sYdJuNwLC1WBDi8MsqPG4urqVYdjCxqYycXlFZxUotMyzE+T/ABokKoqOv0ZM6qCOjMp1B8DXrR11bXPc8aWgtzyO12Vs18NhYIZCpdBJmym4u0hbQkDoIr4L7VTU7oyR9J4XW66+Fkxr5VnqnsYgACyC/Wa9erxOFUEoVLPxOeWnbeXLke4MS5dQdATutbort0fiOpnqYwnhJ9sGdlFarcl2GatX1p5p7qARu1CSfZ+9vD8aEl2gCgCgCgCgCgCgCgCgM08vsebZ0fWMRGR/glrSvczseEYRgZOg9Hsrrgzguj3H21dcGwH7S+0VpZsZablM57BS9HqrODOi2Hc6bY7clu6a0kcsVzOSUFGH9XFYLkzufnR0Gxm5Y/roroRwyjzNlgP6GxB7H/krgv6j0dP7s6bgsbYPDdsMfuisIdJuNTVgeaEnqQ2Fuk0IFW0N6+P4V8p9o354HoaLZlJjXzR3hEwDAndeurR2QrujKzZMrYm4tIsrIHmBXco1Pr/1r367Fq/EFZWvKv5OKSdVDjLdl0mvpzgPvGW06aEHwmhJb2d9Lw/GhJdoAoAoAoAoAoAoAoAoDOfLv+rlPViIvdkFaVdRlcsxMBkQqc4By9JANgT0E7hextXS3hnNFcUcD/GzD4NprdlP2itJSOWuDTZzk8eUhhuP2HqqmMHTF8SwdFsCbMreg3q+TGUeFiEpmTtGo/EVXdGilwywXeDknxgX1eqpiyLo9zbsP+pcR6JP5K5L+o6aPdnTcHx8lw/1MfuisIdJuMke1WBMNOVQFV5CTegIMTFmHaN1eX4n4e9XWuHdG9F3s3zKLYZ+r7R/rXzD8F1eccJ6H3qv4ntMCTziB6NTXbR9nrJe8eDOWtjjEUXIogosBX02l0dWmhw1o4bLJWPMj6xtXQZELNQI+xydBoSMtm/S8PZQku0AUAUAUAUAUAUAUAUBm3lxnBwQiO8yRN74/CtaeowveImM7OxCLh8XFI5DSIiolnOZllRwbg5BzdSwvY8kjUHokstGEJJZIIgThivUwt/pUlXJLmeti4NZ3ELMULhgpy5hnAJGbUZVABJbWwBNjUt4QjHnlH3YblM1+rUdhquzJaUkUYxlP2irrkzNviWSaCIpKGGn0h0W6x9vqNVfLYvHzLDNs2fJm2JiD2S+xK5LdzqrjwxwdbsTzaD6mP3BWMNjQvLrVweXxGtuigPh7KA+UJCmQFCCOWZVtmZVubDMQLk9AvvPZTuMlePErIAyMGU3sR2EqfUQR4VLQXMDUEnkmhIy2NLfMO3X1CgGlAFAFAFAFAFAFAFAFAZB5dJDeNejKh/zyVrT1GGoXkMhmFzcf1212nBHYYuAINP2hUNDOUQ4XFtA5kQIcyNHIrrmUo4swI6LjpBB7dTUSjktXZjkS4KxVusKRfsuKSSZMMrJSZcy6bxqKlrKKQeJDTbE3G4ksJRNmSH4xc3KdYY1cnOAb51beOms4p4NpcpZNU2Wtth4kdkt/Ulctu/0Oup5idjskfJ4Pqo/cFYw2LrYsSyWFh4/6VcEANAehLbU7um+61AypLtqEKWDFgBmIVSSBmy3sbaZrjwPVV1FlXNEE+1mK4gIArwpI3LGYHISA2UEXVsp1uNQR0U4SOIh2h8IfNGjOWUsGIBjvqpjIZbakBrWNtRe40qcJEZbIZNnsjMXkVVJYjNIwZQ5mLDTnC8lwpNuVb6IpxIcDL2z4yECRJJIBms1rCzMW572DWva9ydNdao3k1SwX49mTtvKRjxdvwA+2oJLUew4/ps8neay/wCFLX8b0Aww8CILIoUdSgAfZQEtAFAFAFAFAFAFAFAFAI+EvByHFrlmQMLW6QdCTow1G+mWtg0nyZmu1/JDa5w85X92Vcw7AHWxHqNaq2SMZaeLOX2lwH2hFGy8SsgBzXifMbDqQgMT2Cr+3+Jj92+ByzNa6NcEaMrAgjsIO6t1ZGWzOaVUostbKS2a+6xt21OBkpIdKsij3J8AtnB66jBbOVg2LBN+hMT6Jfdjrjv6vod9HQdfgmtDCP4cfuLXPDpRsFXJC9CGeJxdWF7XUi53C4IvUohizDYSEgKA2IAuMqqZFILO2WRtb2Lk8o9AJ1qzkVUENsPs+QcyFI9MuaRsz2uWsct8wuzHVhqxqrbZdRSLqbIJ+cmc9iARr9l2/wA1QSWcNs2KM3WNQf2iLt/iNyfXQFugCgCgAUB9oAoAoAoAoAoAoAoAoD4aACKAhkwynooBXtXgzh8QLTRRyDozqCR6DvHhQg5HH+SbCtcxGWK/QkhI9T3qVNrYrwRe6OG2z5KdoRXMIXEL0ZSEfxRjb1Ma2je+5hLSrOUzl58JJh2tiIpISP8AeIyC/YSLHwrdXxaOeWnmnsaBgNv4X81TQHExcbJxgSPOM5JVLadA0Op00Ncd80vNvyOyrlHmjvIMbHkjAYMeLjsqXdrZBuVbmsq+lGq2LUcU7cyEjqMjBB6hdh6quSWYtjyH5ya3ZGoFuzO97+oUBai2LANSmc9chMh8M1wPC1AMAKAKAKAKAKAKAKABQH2gCgCgCgCgCgCgCgCgPhoAoAoAoAoAoCltf5lvRQHA8AfOJvH2VWWwO34P/NnvGohsBpVwFAFAFAFAFAFAFAFAFAAoD7QBQBQBQBQH/9k=",
                "description": "UI/UX designing, html css tutorials",
                "content": "Welcome to our channel Dev AT. Here you can learn web designing, UI/UX designing, html css tutorials, css animations and css effects, javascript and jquery tutorials and related so on.",
                "price": 10,
                "colors": ["orange", "black", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "6",
                "title": "React Text Book",
                "src": "https://m.media-amazon.com/images/I/41IiBTZOGCL.jpg",
                "description": "UI/UX designing, html css tutorials",
                "content": "Welcome to our channel Dev AT. Here you can learn web designing, UI/UX designing, html css tutorials, css animations and css effects, javascript and jquery tutorials and related so on.",
                "price": 17,
                "colors": ["orange", "black", "crimson", "teal"],
                "count": 1
            }
        ],

        cart: [],
        total: 0

        };

    addCart = (id) =>{
        const {products, cart} = this.state;
        const check = cart.every(item =>{
            return item._id !== id
        })
        if(check){
            const data = products.filter(product =>{
                return product._id === id
            })
            this.setState({cart: [...cart,...data]})
        }else{
            alert("The product has been added to cart.")
        }
    };

    reduction = id =>{
        const { cart } = this.state;
        cart.forEach(item =>{
            if(item._id === id){
                item.count === 1 ? item.count = 1 : item.count -=1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };

    increase = id =>{
        const { cart } = this.state;
        cart.forEach(item =>{
            if(item._id === id){
                item.count += 1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };

    removeProduct = id =>{
        if(window.confirm("Do you want to delete this product?")){
            const {cart} = this.state;
            cart.forEach((item, index) =>{
                if(item._id === id){
                    cart.splice(index, 1)
                }
            })
            this.setState({cart: cart});
            this.getTotal();
        }

    };

    getTotal = ()=>{
        const{cart} = this.state;
        const res = cart.reduce((prev, item) => {
            return prev + (item.price * item.count);
        },0)
        this.setState({total: res})
    };

    componentDidUpdate(){
        localStorage.setItem('dataCart', JSON.stringify(this.state.cart))
        localStorage.setItem('dataTotal', JSON.stringify(this.state.total))
    };

    componentDidMount(){
        const dataCart = JSON.parse(localStorage.getItem('dataCart'));
        if(dataCart !== null){
            this.setState({cart: dataCart});
        }
        const dataTotal = JSON.parse(localStorage.getItem('dataTotal'));
        if(dataTotal !== null){
            this.setState({total: dataTotal});
        }

    }


    render() {
        const {products, cart,total} = this.state;
        const {addCart,reduction,increase,removeProduct,getTotal} = this;
        return (
            <DataContext.Provider
            value={{products, addCart, cart, reduction,increase,removeProduct,total,getTotal}}>
                {this.props.children}
            </DataContext.Provider>
        )
    }
}

